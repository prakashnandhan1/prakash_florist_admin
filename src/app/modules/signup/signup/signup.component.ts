import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { MustMatch } from '../../../helpers/password_validator';
import {SignupService} from '../../../services/signup'
@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss']
})
export class SignupComponent implements OnInit {
  signup_form:FormGroup;
  submitted = false;
  pshow:boolean;
  cshow:boolean;
  


  constructor(private fb: FormBuilder, private signup_service:SignupService) { }
  ngOnInit(): void {
    this.signup_form = this.fb.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(6)]],
      username:['', [Validators.required]],
      CPassword:['', [Validators.required,Validators.minLength(6)]],
    },
    {
      validator: MustMatch('password','CPassword')
  }  
    ) 
  }
  password() {
    this.pshow = !this.pshow;
  }
  cpassword()
  {
    this.cshow = !this.cshow;
  }

  get f() { return this.signup_form.controls; }

  onSubmit1() {
    this.submitted = true;


    if (this.signup_form.invalid) {
      return;
    }
{
  this.signup_service.save_signup(this.signup_form.value).subscribe(res=>{
    console.log(res);
  });

  // alert('SUCCESS!! :-)\n\n' + JSON.stringify(this.signup_form.value))


}

}
}
