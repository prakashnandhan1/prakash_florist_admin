import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
// import { environment } from 'src/environments/environment';



@Injectable({
  providedIn: 'root'
})
export class SignupService {
  // onboardingUrl ="http://rider-club.dxminds.online:5555/api/onboarding"
//  onboardingUrl = environment.url + '/api/onboarding'
onboardingUrl = "http://localhost:5555/api/onboarding"
  constructor( private http: HttpClient) { }

save_signup(form){
console.log("data",form)
return this.http.post(this.onboardingUrl + '/create',form)
}

}